__author__ = 'kasim.otkun'
# -*- coding: utf-8 -*-
import PageObjects
from random import randint
import time

class TestCase(PageObjects.BaseTestCase):
    def setUp(self):
        self.skip_reasons = [55] # bug tracker issue numbers
        PageObjects.BaseTestCase.setUp(self)
        self.randomString = "Test" + str(randint(1234,956655662))

    def test_1_searchIssue(self):
        homePage = PageObjects.HomePage(self.tc)  # pass in test context, such as the driver object and useful variables
        loginPage = homePage.goto_loginPage()
        homePage = loginPage.login("kasimotkun@gmail.com","J1r@2014")
        searchText = "testkasimotkun"
        issueListPage = homePage.search_issueByText(searchText)
        assert searchText == issueListPage.get_issueSummary()

    def test_2_createIssue(self):
        homePage = PageObjects.HomePage(self.tc)  # pass in test context, such as the driver object and useful variables
        loginPage = homePage.goto_loginPage()
        homePage = loginPage.login("kasimotkun@gmail.com","J1r@2014")
        newissuePage = homePage.create_issue()
        newissuePage.createUpdateIssue(self.randomString,"Bug","Automatic","Trivial","testDesc")
        time.sleep(3)
        issueListPage = homePage.search_issueByText(self.randomString)
        assert self.randomString == issueListPage.get_issueSummary()


    def test_3_updateIssue(self):
        homePage = PageObjects.HomePage(self.tc)  # pass in test context, such as the driver object and useful variables
        loginPage = homePage.goto_loginPage()
        homePage = loginPage.login("kasimotkun@gmail.com","J1r@2014")
        issueListPage = homePage.search_issueByText(self.randomString)
        newissuePage = issueListPage.edit_issue()
        self.randomString = self.randomString+"Updated"
        newissuePage.createUpdateIssue(self.randomString,None,None,None,None,True)
        issueListPage = homePage.search_issueByText(self.randomString)
        assert self.randomString == issueListPage.get_issueSummary()
