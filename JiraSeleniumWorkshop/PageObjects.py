# -*- coding: utf-8 -*-
from ConfigParser import SafeConfigParser
from nose.plugins.skip import SkipTest
from selenium import webdriver
from selenium.webdriver.support import wait
from selenium.webdriver.common.action_chains import ActionChains
import sys
import unittest
import time
from selenium.webdriver.common.keys import Keys


from selenium.webdriver.support.ui import Select

class TestContext(object):
    def __init__(self):
        self._get_configuration()
        self.driver = None

    def _get_configuration(self):
        config_parser = SafeConfigParser()
        config_parser.read(r"setup.cfg")
        #settings = dict(config_parser.items('settings'))
        self.driver_to_use = 'Firefox'  #settings['browser']

    def open_browser(self):
        if(self.driver_to_use == 'Chrome'):
            self.driver = webdriver.Chrome()
        else:
            self.driver = webdriver.Firefox()

class BaseTestCase(unittest.TestCase):
    def skip_on_known_bug(self):
        """ issues commented out means it's fixed """
        known_issues = {
            101: 'Browser crash',
            #102: 'Firefox freeze',
        }
        for reason in self.skip_reasons:
            if known_issues.get(reason, None):
                raise SkipTest(reason, known_issues[reason])

    def setUp(self):
        unittest.TestCase.setUp(self)
        self.skip_on_known_bug()
        self.tc = TestContext() # used by all tests and Page Object classes
        self.tc.open_browser()
        self.tc.user_url = "https://jira.atlassian.com"
        self.tc.driver.get(self.tc.user_url)
        wait.WebDriverWait(self.tc.driver,3000)

    def tearDown(self):
        if sys.exc_info() == (None, None, None): # leave window open on fail
            self.tc.driver.quit()

class HomePage(BaseTestCase):
    def __init__(self, context):
        self.tc = context  # make username, web driver, etc. available to the page object's methods

    def goto_loginPage(self):
        self.tc.driver.find_element_by_link_text("Log In").click()
        wait.WebDriverWait(self.tc.driver,3000)
        time.sleep(1)
        return LoginPage(self.tc)

    def create_issue(self):
        self.tc.driver.find_element_by_id("create_link").click()
        wait.WebDriverWait(self.tc.driver,3000)
        time.sleep(5)
        return NewIssuePage(self.tc)

    def search_issueByText(self,searchText):
        self.tc.driver.find_element_by_id("quickSearchInput").clear()
        self.tc.driver.find_element_by_id("quickSearchInput").send_keys(searchText)
        self.tc.driver.find_element_by_id("quickSearchInput").send_keys(Keys.ENTER)
        wait.WebDriverWait(self.tc.driver,3000)
        time.sleep(10)
        return IssueListPage(self.tc)
    def search_issueById(self,issueId):
        self.tc.driver.find_element_by_id("quickSearchInput").clear()
        self.tc.driver.find_element_by_id("quickSearchInput").send_keys(issueId)
        self.tc.driver.find_element_by_id("quickSearchInput").send_keys(Keys.ENTER)
        time.sleep(10)
        return IssueDetailsPage(self.tc)


class LoginPage(BaseTestCase):
    def __init__(self, context):
        self.tc = context  # make username, web driver, etc. available to the page object's methods

    def login(self,username,password):
        self.tc.driver.find_element_by_id("username").clear()
        self.tc.driver.find_element_by_id("username").send_keys(username)
        self.tc.driver.find_element_by_id("password").clear()
        self.tc.driver.find_element_by_id("password").send_keys(password)
        self.tc.driver.find_element_by_id("rememberMe").click()
        self.tc.driver.find_element_by_id("login-submit").click()
        wait.WebDriverWait(self.tc.driver,3000)
        time.sleep(10)
        return HomePage(self.tc)

class IssueListPage(BaseTestCase):
    def __init__(self, context):
        self.tc = context  # make username, web driver, etc. available to the page object's methods

    def get_issueSummary(self):
        return self.tc.driver.find_element_by_id("summary-val").text
    def edit_issue(self):
        self.tc.driver.find_element_by_css_selector("span.trigger-label").click()
        time.sleep(10)
        return NewIssuePage(self.tc)


class NewIssuePage(BaseTestCase):
    def __init__(self, context):
        self.tc = context  # make username, web driver, etc. available to the page object's methods

    def createUpdateIssue(self,summary=None,issueType=None,assigned=None,priority=None,desc=None,isUpdate=False):
        if (issueType != None):
            self.tc.driver.find_element_by_xpath("//div[@id='issuetype-single-select']/span").click()
            self.tc.driver.find_element_by_link_text(issueType).click()
        time.sleep(10)
        if (summary!=None):
            self.tc.driver.find_element_by_id("summary").clear()
            self.tc.driver.find_element_by_id("summary").send_keys(summary)
        Select(self.tc.driver.find_element_by_id("security")).select_by_visible_text("Logged-in users")
        if (priority!=None):
            self.tc.driver.find_element_by_xpath("//div[@id='priority-single-select']/span").click()
            self.tc.driver.find_element_by_link_text(priority).click()
        self.tc.driver.find_element_by_xpath("//div[@id='components-multi-select']/span").click()
        self.tc.driver.find_element_by_link_text("Lee 2").click()
        if (assigned!=None):
            self.tc.driver.find_element_by_xpath("//div[@id='assignee-single-select']/span").click()
            self.tc.driver.find_element_by_id("assignee-field").clear()
            self.tc.driver.find_element_by_id("assignee-field").send_keys(assigned)

        if (desc!=None):
            self.tc.driver.find_element_by_id("description").clear()
            self.tc.driver.find_element_by_id("description").send_keys(desc)
        if (isUpdate == False):
            self.tc.driver.find_element_by_id("create-issue-submit").click()
        if (isUpdate == True):
            self.tc.driver.find_element_by_id("edit-issue-submit").click()
        wait.WebDriverWait(self.tc.driver,3000)
        time.sleep(10)
        return HomePage(self.tc)


class IssueDetailsPage(BaseTestCase):
    def __init__(self, context):
        self.tc = context  # make username, web driver, etc. available to the page object's methods

    def get_issueSummary(self):
        return self.tc.driver.find_element_by_id("summary-val").text
